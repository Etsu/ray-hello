from ray import serve
from starlette.requests import Request


@serve.deployment()
class Hello:
    async def __call__(self, request: Request) -> str:
        try:
            request_json = await request.json()
            name = request_json["name"]
        except:
            name = "anonymous"
        return f"Hello, {name}! (verison 0.0.4)"

dag = Hello.bind()
